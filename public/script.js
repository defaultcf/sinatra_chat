document.addEventListener("DOMContentLoaded", () => {
  const form = document.querySelector("form");
  const messages = document.querySelector("#messages");

  form.onsubmit = async e => { // "=>"はアロー関数の文法
    e.preventDefault(); // デフォのPOSTが動かないようにする

    // フォームのデータを作り上げる
    const formData = new FormData();
    const message = form.message.value;
    if (message === "") return; // 中身が空なら終了する
    formData.append("message", message);

    // POSTする
    const row = await fetch(location.href, {
      method: "POST",
      body: formData,
    });

    // POSTに成功したら、メッセージを下に追加する
    if (row.status === 200) {
      const new_message = document.createTextNode(message);
      const new_message_element = document.createElement("li");
      new_message_element.appendChild(new_message);
      messages.appendChild(new_message_element);
    }
  }
});
