require "rubygems"
require "bundler/setup"
Bundler.require

ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'db.sqlite3')

class Chat < ActiveRecord::Base
end

get "/room/:name" do
  @name = params[:name]
  @chats = Chat.where(name: @name)
  haml :room
end

post "/room/:name" do
  name = params[:name]
  message = params[:message]
  if message != ""
    Chat.create(name: name, text: message)
  end
  redirect "/room/#{name}"
end
